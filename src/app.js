import React, {PureComponent} from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import './app.css';
import Layout from "./layout";
import Dashboard from "./dashboard";

export class App extends PureComponent {
  render() {
    return (
      <Router>
        <Layout>
          <Route exact path="/" component={Dashboard}/>
        </Layout>
      </Router>
    );
  }
}

export default App;
