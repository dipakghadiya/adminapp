import React from 'react';
import Header from "./header";
import "./app.css";

export class Layout extends React.Component {
  render() {
    return (
      <div>
        <Header/>
        <div className="app app-section">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Layout;