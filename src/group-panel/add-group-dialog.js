import React, {PureComponent} from 'react';
import TextField from '@material-ui/core/TextField';
import {connect} from "react-redux";
import {addGroup, editGroup} from "../redux/actions/group-actions";
import FormDialog from "../common/form-dialog";


export class AddGroupDialog extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      formData: {
        id: (new Date()).getTime(),
        name: ''
      },
      validationError: ''
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {record} = nextProps;
    if (record) {
      this.setState({formData: record});
    } else {
      this.setState({
        formData: {
          id: (new Date()).getTime(),
          name: ''
        }
      });
    }
  }

    handleFieldChange = (field, value) => {
      let formData = {...this.state.formData};
      formData[field] = value;
      this.setState({formData});
      this.setState({validationError: ''});
    };

    validateFormData = (formData) => {
      if (formData.name === "") {
        this.setState({validationError: 'Group name is required'});
        return false;
      }
      return true;
    };

    handleSave = () => {
      const {action, editGroup, addGroup, handleClose} = this.props;
      const {formData} = this.state;

      if (this.validateFormData(formData)) {
        if (action === 'edit') {
          editGroup(formData);
        } else {
          addGroup(formData);
        }
        handleClose();
      }
    };

    render() {
      const {open, handleClose} = this.props;
      const {formData: {name}, validationError} = this.state;
      return <FormDialog
        open={open}
        handleClose={handleClose}
        handleSave={this.handleSave}
        title="Group"
        validationError={validationError}>
        <TextField
          margin="dense"
          id="name"
          label="Group Name"
          type="text"
          value={name}
          required
          onChange={(event) => this.handleFieldChange('name', event.target.value)}
          fullWidth
        />
      </FormDialog>;
    }

}


const mapStateToProps = state => ({
  data: state.groupReducer.groups
});
export default connect(mapStateToProps, {addGroup, editGroup})(AddGroupDialog);