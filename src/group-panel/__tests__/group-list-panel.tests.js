import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import {userList} from "../../data/user-data";
import {groupList} from "../../data/group-data";
import {GroupListPanel} from "../group-list-panel";

const mockFunction = jest.fn();

describe('Snapshot testing', () => {

  it('render list correctly for no data', () => {
    const wrapper = shallow(<GroupListPanel searchValue={''}
      openDialog={mockFunction}
      deleteGroup={mockFunction}
      data={[]}
      usersData={[]}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render list correctly for no search data', () => {
    const wrapper = shallow(<GroupListPanel searchValue={'search'}
      openDialog={mockFunction}
      deleteGroup={mockFunction}
      data={[]}
      usersData={[]}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render list correctly for data', () => {
    const wrapper = shallow(<GroupListPanel searchValue={''}
      openDialog={mockFunction}
      deleteGroup={mockFunction}
      data={groupList}
      usersData={userList}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});

describe('Function testing', () => {
  let wrapper, instance;
  beforeEach(() => {
    wrapper = shallow(<GroupListPanel searchValue={''}
      openDialog={mockFunction}
      deleteGroup={mockFunction}
      data={groupList}
      usersData={userList}
    />);
    instance = wrapper.instance();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
  it('onDeleteClick for validation error', () => {
    const event = {
      stopPropagation: jest.fn()
    };
    const group = {id: 1, name: "Test"};
    instance.isNoUserPresentInGroup = jest.fn();
    instance.isNoUserPresentInGroup.mockReturnValue(false);
    instance.onDeleteClick(event, group);
    expect(mockFunction).toHaveBeenCalledTimes(0);
    expect(instance.state.alertError).toEqual('Group can not be deleted as it assigned to some users');
  });

  it('onDeleteClick for no validation error', () => {
    const event = {
      stopPropagation: jest.fn()
    };
    const group = {id: 1, name: "Test"};
    instance.isNoUserPresentInGroup = jest.fn();
    instance.isNoUserPresentInGroup.mockReturnValue(true);
    instance.onDeleteClick(event, group);
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(mockFunction).toHaveBeenCalledWith(group);

  });

  it('onEditClick', () => {
    const event = {
      stopPropagation: jest.fn()
    };
    const group = {id: 1, name: "Test"};
    instance.onEditClick(event, group);
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(mockFunction).toHaveBeenCalledWith('group', group, 'edit');
  });
});
