import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import {AddGroupDialog} from "../add-group-dialog";

const mockFunction = jest.fn();

describe('Snapshot testing', () => {

  it('render dialog for open', () => {
    const wrapper = shallow(<AddGroupDialog open={true}
      record={{}}
      handleClose={mockFunction}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render dialog for close', () => {
    const wrapper = shallow(<AddGroupDialog open={false}
      record={{}}
      handleClose={mockFunction}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render dialog for validationError', () => {
    const wrapper = shallow(<AddGroupDialog open={false}
      record={{}}
      handleClose={mockFunction}/>);
    wrapper.setState({validationError:'Some Error'});
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});

describe('Function testing', () => {
  let wrapper, instance;
  const formData = {id:1, name:"Group1"};
  let editGroupMock = jest.fn();
  let addGroupMock = jest.fn();
  beforeEach(() => {
    wrapper = shallow(<AddGroupDialog open={false}
      record={formData}
      handleClose={mockFunction}
      editGroup={editGroupMock}
      addGroup={addGroupMock}/>);
    instance = wrapper.instance();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
  it('handleFieldChange', () => {
    instance.handleFieldChange('name', 'Group2');
    expect(instance.state.formData.name).toEqual('Group2');
    expect(instance.state.validationError).toEqual('');
  });

  it('validateFormData for no validation error', () => {
    const response  = instance.validateFormData(formData);
    expect(response).toEqual(true);

  });

  it('validateFormData for validation error', () => {
    const response  = instance.validateFormData({id:1, name:''});
    expect(response).toEqual(false);
    expect(instance.state.validationError).toEqual('Group name is required');
  });

  it('handleSave for validation error', () => {
    instance.validateFormData = jest.fn();
    instance.validateFormData.mockReturnValue(false);
    instance.handleSave();
    expect(mockFunction).toHaveBeenCalledTimes(0);
    expect(addGroupMock).toHaveBeenCalledTimes(0);
    expect(editGroupMock).toHaveBeenCalledTimes(0);
  });

  it('handleSave for add and no validation error', () => {
    wrapper.setProps({action:'add'});
    instance.validateFormData = jest.fn();
    instance.validateFormData.mockReturnValue(true);
    instance.handleSave();
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(addGroupMock).toHaveBeenCalledTimes(1);
    expect(addGroupMock).toHaveBeenCalledWith(formData);
    expect(editGroupMock).toHaveBeenCalledTimes(0);
  });

  it('handleSave for edit and no validation error', () => {
    wrapper.setProps({action:'edit'});
    instance.validateFormData = jest.fn();
    instance.validateFormData.mockReturnValue(true);
    instance.handleSave();
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(addGroupMock).toHaveBeenCalledTimes(0);
    expect(editGroupMock).toHaveBeenCalledTimes(1);
    expect(editGroupMock).toHaveBeenCalledWith(formData);
  });
});
