import React, {PureComponent} from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import TagArea from "../common/tag-area";
import ListItemSummary from "../common/list-item-summary";
import "./group-list-panel.css";
import {connect} from "react-redux";
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import {deleteGroup} from "../redux/actions/group-actions";
import AlertDialog from "../common/alert-dialog";


export class GroupListPanel extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      expanded: '',
      alertError: ''
    };
  }

    handleChange = panel => (event, newExpanded) => {
      this.setState({expanded: newExpanded ? panel : false});
    };

    onEditClick = (event, group) => {
      event.stopPropagation();
      this.props.openDialog('group', group, 'edit');
    };

    isNoUserPresentInGroup = (group) => {
      const {usersData} = this.props;
      return usersData.filter(user=>user.groups.includes(group.id)).length === 0;
    };

    onDeleteClick = (event, group) => {
      event.stopPropagation();
      if(this.isNoUserPresentInGroup(group)){
        this.props.deleteGroup(group);
      } else {
        this.setState({alertError: 'Group can not be deleted as it assigned to some users'});
      }
    };

    alertClose = () => {
      this.setState({alertError: ''});
    };

    render() {
      const {data: groupList, searchValue, usersData} = this.props;
      const {expanded, alertError} = this.state;
      const data = groupList.filter(group=> group.name.toLowerCase().includes(searchValue.toLowerCase()));
      const noDataMessage = searchValue ? 'No search result present' : 'No Data available';

      return (
        <React.Fragment>
          {data.length > 0 ?
            data.map(group => {
              const users = usersData.filter(user=>user.groups.includes(group.id));
              return <ExpansionPanel key={group.name} square expanded={expanded === group.name}
                onChange={this.handleChange(group.name)}>
                <ExpansionPanelSummary id="panel1d-header" className="item-summary">
                  <Edit onClick={(event) => this.onEditClick(event, group)}/>
                  <Delete onClick={(event) => this.onDeleteClick(event, group)}/>
                  <ListItemSummary title={group.name} />
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className="item-details">
                  <TagArea data={users} label="Users in Group"/>
                </ExpansionPanelDetails>
              </ExpansionPanel>;

            }): <span className="no-data-message">{noDataMessage}</span>}
          <AlertDialog message={alertError} handleClose={this.alertClose}/>
        </React.Fragment>
      );
    }

}

const mapStateToProps = state =>({
  data: state.groupReducer.groups,
  usersData: state.userReducer.users
});
export default connect(mapStateToProps, {deleteGroup})(GroupListPanel);