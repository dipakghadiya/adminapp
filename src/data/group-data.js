export const groupList = [{
  id: 1,
  name: "Admin"
}, {
  id: 2,
  name: "Supervisor"
}, {
  id: 3,
  name: "Delegate"
}, {
  id: 4,
  name: "Owner"
}];