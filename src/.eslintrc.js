module.exports = {
    "extends": "react-app",
    "rules" : {
        "no-unused-vars": 2,
        "no-unreachable": 2,
        "no-const-assign":1,
        "semi": [1, "always"],
    }
};