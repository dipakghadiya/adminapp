import React, {PureComponent} from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Person from '@material-ui/icons/Person';
import Group from '@material-ui/icons/Group';
import PersonAdd from '@material-ui/icons/PersonAdd';
import GroupAdd from '@material-ui/icons/GroupAdd';
import Button from '@material-ui/core/Button';
import "./tab-menu.css";
import SearchBox from "./search-box";

export default class TabMenu extends PureComponent {

  render() {
    const {openDialog, value, searchValue, onSearch, handleChange} = this.props;
    return (
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        className="operations-tabs"
      >
        <Tab label="Users" icon={<Person/>}/>
        <Tab label="Groups" icon={<Group/>}/>
        <div className="operations-action">
          <SearchBox onChange={onSearch} value={searchValue}/>
          {value === 0 ?
            <Button variant="contained" color="primary" onClick={() => openDialog("user")}>
              <PersonAdd/>
            </Button> :
            <Button variant="contained" color="primary" onClick={() => openDialog("group")}>
              <GroupAdd/>
            </Button>}
        </div>
      </Tabs>
    );
  }

}