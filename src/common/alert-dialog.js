import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';

const AlertDialog = ({message, handleClose}) => {
  return (
    <Dialog
      open={message !== ''}
      onClose={handleClose}
    >
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
                    OK
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AlertDialog;