import React, {PureComponent} from 'react';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


export default class CheckboxGroup extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            selectedData: props.selectedData
        };
    }

    handleChange = id => {
        let data = [...this.state.selectedData];
        if (data.includes(id)) {
            data = data.filter(item => item !== id);
        } else {
            data.push(id);
        }
        this.setState({selectedData: data});
        this.props.onChange(data);
    };

    render() {
        const {data} = this.props;
        const {selectedData} = this.state;

        return <FormControl required component="fieldset">
            <FormLabel component="legend">Groups</FormLabel>
            <FormGroup>
                {data.map((item) => {
                    return (<FormControlLabel
                            key={item.id}
                            control={<Checkbox checked={selectedData.includes(item.id)}
                                               onChange={() => this.handleChange(item.id)}
                                               value={item.id}/>}
                            label={item.name}
                        />
                    );
                })}
            </FormGroup>
        </FormControl>;
    }

}
