import React from 'react';
import "./list-item-summary.css";

const ListItemSummary = ({title, description}) => {

  return (
    <div className="summary-section">
      <span className="title">{title}</span>
      {description && <span className="description">{description}</span>}
    </div>
  );
};
export default ListItemSummary;