import React, {PureComponent} from 'react';
import "./tag-area.css";
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import SearchBox from "./search-box";

export default class TagArea extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      searchString: ''
    };
  }

    onSearchChange = (event) => {
      const value = (event.target.value || "").trim();
      this.setState({searchString: value});
    };

    filterData = (searchString, data) => {
      return searchString ?
        data.filter(item => item && item.name.toLowerCase().includes(searchString.toLowerCase())) : data;
    };

    render() {
      const {label, theme, data} = this.props;
      const {searchString} = this.state;
      const filteredData = this.filterData(searchString, data);
      const noDataMessage = searchString ? 'No search result present' : 'No Data available';
      return (
        <React.Fragment>
          <Typography variant='subtitle1' className='contains-area-label'>
            {label && `${label} :`}
            <SearchBox onChange={this.onSearchChange}/>
          </Typography>
          <div className={`contains-area ${theme}`}>
            {
              !filteredData || filteredData.length === 0 ? noDataMessage :
                filteredData.map((item) => {
                  return <Chip key={item.id} label={item.name} className='chip'/>;
                })
            }
          </div>
        </React.Fragment>
      );
    }
}