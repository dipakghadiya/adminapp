import React, {PureComponent} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import "./form-dialog.css";

export class FormDialog extends PureComponent {


  render() {
    const {open, handleClose, handleSave, title, validationError} = this.props;

    return <Dialog open={open} onClose={handleClose} disableBackdropClick
      disableEscapeKeyDown className="dialog-container">
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        {this.props.children}
        {validationError !== '' && <div className="error-message">{validationError}</div>}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
                    Cancel
        </Button>
        <Button onClick={handleSave} color="primary">
                    Save
        </Button>
      </DialogActions>
    </Dialog>;
  }

}

export default FormDialog;