import React from 'react';
import "./search-box.css";
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

const SearchBox = ({value, onChange}) => {
  return (
    <Input
      id="adornment-weight"
      value={value}
      placeholder="Search..."
      className='search-box'
      startAdornment={<InputAdornment position="start"><SearchIcon/></InputAdornment>}
      aria-describedby="weight-helper-text"
      onChange={onChange}
    />
  );
};

export default SearchBox;
