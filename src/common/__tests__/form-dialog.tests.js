import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import FormDialog from "../form-dialog";

describe('Snapshot testing', () => {

  const mockFunction = jest.fn();

  it('render form dialog for open', () => {
    const wrapper = shallow(<FormDialog open={true}
      handleClose={mockFunction}
      handleSave={mockFunction}
      title="Test"
      validationError={''}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render form dialog for close', () => {
    const wrapper = shallow(<FormDialog open={false}
      handleClose={mockFunction}
      handleSave={mockFunction}
      title="Test"
      validationError={''}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render form dialog for validation error ', () => {
    const wrapper = shallow(<FormDialog open={true}
      handleClose={mockFunction}
      handleSave={mockFunction}
      title="Test"
      validationError={'Some error'}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});