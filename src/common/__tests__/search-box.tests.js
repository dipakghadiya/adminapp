import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import SearchBox from "../search-box";

describe('Snapshot testing', () => {

  it('render search box correctly with no value', () => {
    const wrapper = shallow(<SearchBox value={''} onChange={jest.fn()}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render search box correctly with value', () => {
    const wrapper = shallow(<SearchBox value={'Test'} onChange={jest.fn()}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});