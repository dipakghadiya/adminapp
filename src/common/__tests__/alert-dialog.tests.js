import React from 'react';
import AlertDialog from '../alert-dialog';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Snapshot testing', () => {

  it('render alert correctly with no message', () => {
    const wrapper = shallow(<AlertDialog message='' handleClose={jest.fn()}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('check alert correctly with message', () => {
    const wrapper = shallow(<AlertDialog message='Alert message' handleClose={jest.fn()}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});