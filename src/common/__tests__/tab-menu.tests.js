import React from 'react';
import TabMenu from '../tab-menu';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Snapshot testing', () => {

  const mockFunction = jest.fn();

  it('render tab menu correctly for first tab', () => {
    const wrapper = shallow(<TabMenu openDialog={mockFunction}
      value={0}
      searchValue={''}
      onSearch={mockFunction}
      handleChange={mockFunction}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render tab menu correctly for other tab', () => {
    const wrapper = shallow(<TabMenu openDialog={mockFunction}
      value={1}
      searchValue={''}
      onSearch={mockFunction}
      handleChange={mockFunction}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});