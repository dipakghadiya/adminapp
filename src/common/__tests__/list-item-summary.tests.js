import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import ListItemSummary from "../list-item-summary";

describe('Snapshot testing', () => {

  it('render alert correctly with description', () => {
    const wrapper = shallow(<ListItemSummary title='Title' description='Description'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render alert correctly with no description', () => {
    const wrapper = shallow(<ListItemSummary title='Title'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});