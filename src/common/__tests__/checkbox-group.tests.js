import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import CheckboxGroup from "../checkbox-group";

const mockFunction = jest.fn();
const mockData = [{id:1, name:"Test"}, {id:2, name:"Test1"}];

describe('Snapshot testing', () => {

  it('render tab checkbox group for blank data', () => {
    const wrapper = shallow(<CheckboxGroup data={[]}
      selectedData={[]}
      onChange={mockFunction}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render tab checkbox group for blank selected data', () => {
    const wrapper = shallow(<CheckboxGroup data={mockData}
      selectedData={[]}
      onChange={mockFunction}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render tab checkbox group for some selected data', () => {
    const wrapper = shallow(<CheckboxGroup data={mockData}
      selectedData={[1]}
      onChange={mockFunction}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});

describe('Function testing', () => {


  let wrapper, instance;
  beforeEach(()=>{
    wrapper  = shallow(<CheckboxGroup data={mockData}
      selectedData={[1]}
      onChange={mockFunction}/>);
    instance = wrapper.instance();
  });

  afterEach(()=>{
    jest.clearAllMocks();
  });
  it('handleChange should add element not present in selected array', () => {
    const expected = [1,2];
    instance.handleChange(2);
    expect(instance.state.selectedData).toEqual(expected);
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(mockFunction).toHaveBeenCalledWith(expected);
  });
  it('handleChange should remove element if present in selected array', () => {
    const expected = [];
    instance.handleChange(1);
    expect(instance.state.selectedData).toEqual(expected);
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(mockFunction).toHaveBeenCalledWith(expected);
  });
});