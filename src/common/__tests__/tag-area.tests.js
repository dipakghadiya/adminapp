import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import TagArea from "../tag-area";

const mockData = [{id:1, name:"Test"}, {id:2, name:"Test1"}];

describe('Snapshot testing', () => {

  it('render tag area correctly', () => {
    const wrapper = shallow(<TagArea data={mockData} label="TestLabel"/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render tag area correctly for no data', () => {
    const wrapper = shallow(<TagArea data={[]} label="TestLabel"/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render tag area correctly for no data for search', () => {
    const wrapper = shallow(<TagArea data={[]} label="TestLabel"/>);
    wrapper.setState({searchString:"search"});
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});

describe('Function testing', () => {


  let wrapper, instance;
  beforeEach(()=>{
    wrapper  = shallow(<TagArea data={mockData} label="TestLabel"/>);
    instance = wrapper.instance();
  });

  afterEach(()=>{
    jest.clearAllMocks();
  });
  it('onSearchChange set state for blank value', () => {
    const event = {
      target:{
        value:''
      }
    };
    instance.onSearchChange(event);
    expect(instance.state.searchString).toEqual('');
  });
  it('onSearchChange set state for some value', () => {
    const event = {
      target:{
        value:'search'
      }
    };
    instance.onSearchChange(event);
    expect(instance.state.searchString).toEqual('search');
  });

  it('filterData for blank search', () => {
    let response = instance.filterData('',mockData);
    expect(response).toEqual(mockData);
  });

  it('filterData for some search', () => {
    let response = instance.filterData('Test1',mockData);
    expect(response).toEqual([{id:2, name:"Test1"}]);
  });
});
