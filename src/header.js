import React, {PureComponent} from 'react';
import {Link} from "react-router-dom";
import './app.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

export class Header extends PureComponent {
  render() {
    return (
      <AppBar position="static">
        <Toolbar>
          <Link to='/' className="header-link">
            <Typography variant="h5">
                            User Management
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
    );
  }
}

export default Header;
