import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import Dashboard from "../dashboard";

describe('Snapshot testing', () => {

  it('render dashboard for first tab', () => {
    const wrapper = shallow(<Dashboard />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render dashboard for next tab', () => {
    const wrapper = shallow(<Dashboard />);
    wrapper.setState({value: 1});
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});

describe('Function testing', () => {
  let wrapper, instance;
  beforeEach(() => {
    wrapper = shallow(<Dashboard />);
    instance = wrapper.instance();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
  it('closeDailog', () => {
    instance.closeDailog();
    expect(instance.state.openDialog).toEqual(false);
    expect(instance.state.type).toEqual('');
    expect(instance.state.action).toEqual('');
    expect(instance.state.record).toEqual(undefined);
  });

  it('openDialog', () => {
    const record = {id:1, name:"Test"};
    instance.openDialog("user", record, "add");
    expect(instance.state.openDialog).toEqual(true);
    expect(instance.state.type).toEqual('user');
    expect(instance.state.action).toEqual('add');
    expect(instance.state.record).toEqual(record);
  });

  it('onSearch', () => {
    const event = {target:{value:"Test"}};
    instance.onSearch(event);
    expect(instance.state.searchValue).toEqual("Test");
  });

  it('handleChange for undefined newValue', () => {
    const event = {target:{value:"Test"}};
    instance.handleChange(event, undefined);
    expect(instance.state.value).toEqual(0);
  });

  it('handleChange for some newValue', () => {
    const event = {target:{value:"Test"}};
    instance.handleChange(event, 1);
    expect(instance.state.value).toEqual(1);
    expect(instance.state.searchValue).toEqual("");
  });
});
