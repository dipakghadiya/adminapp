import React from 'react';
import {Layout} from '../layout';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Snapshot testing', () => {
  it('renders layout with no child', () => {
    const wrapper = shallow(<Layout/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders layout with some child', () => {
    const wrapper = shallow(<Layout><span>Test Child</span></Layout>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});