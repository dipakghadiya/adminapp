import React from 'react';
import {App} from '../app';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Snapshot testing', () => {
  it('renders app correctly', () => {
    const wrapper = shallow(<App/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});