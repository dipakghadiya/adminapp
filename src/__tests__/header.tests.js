import React from 'react';
import {Header} from '../header';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';

import {Link} from "react-router-dom";

describe('Snapshot testing', () => {

  it('render header correctly', () => {
    const wrapper = shallow(<Header/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('check header has a dashboard link inside', () => {
    const wrapper = shallow(<Header/>);
    expect(wrapper.find(Link).props().to).toBe('/');
  });
});