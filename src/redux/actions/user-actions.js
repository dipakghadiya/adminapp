export const addUser = (user) => dispatch => {
  dispatch({
    type: 'ADD_USER',
    payload: user
  });
};

export const editUser = (user) => dispatch => {
  dispatch({
    type: 'EDIT_USER',
    payload: user
  });
};

export const deleteUser = (user) => dispatch => {
  dispatch({
    type: 'DELETE_USER',
    payload: user
  });
};