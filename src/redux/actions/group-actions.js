export const addGroup = (group) => dispatch => {
  dispatch({
    type: 'ADD_GROUP',
    payload: group
  });
};

export const editGroup = (group) => dispatch => {
  dispatch({
    type: 'EDIT_GROUP',
    payload: group
  });
};

export const deleteGroup = (group) => dispatch => {
  dispatch({
    type: 'DELETE_GROUP',
    payload: group
  });
};