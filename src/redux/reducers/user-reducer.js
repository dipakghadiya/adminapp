import {userList} from "../../data/user-data";

const defaultState={users:userList};
const userReducer = (state = defaultState, action) => {
  switch (action.type) {
  case 'ADD_USER':
    return {
      users: [...state.users,action.payload]
    };
  case 'EDIT_USER':{
    let userToUpdate = action.payload;
    let users = state.users.map(user=>
      user.id === userToUpdate.id ? userToUpdate : user
    );
    return {
      users: users
    };
  }
  case 'DELETE_USER':{
    let userToUpdate = action.payload;
    let users = state.users.filter(user=>user.id !== userToUpdate.id);
    return {
      users: users
    };
  }
  default:
    return state;
  }
};

export default userReducer;