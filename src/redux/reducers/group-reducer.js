import {groupList} from "../../data/group-data";

const defaultState={groups:groupList};
const groupReducer = (state = defaultState, action) => {
  switch (action.type) {
  case 'ADD_GROUP':
    return {
      groups: [...state.groups,action.payload]
    };
  case 'EDIT_GROUP':{
    let groupToUpdate = action.payload;
    let groups = state.groups.map(group=>
      group.id === groupToUpdate.id ? groupToUpdate : group
    );
    return {
      groups: groups
    };
  }
  case 'DELETE_GROUP':{
    let groupToUpdate = action.payload;
    let groups = state.groups.filter(group=>group.id !== groupToUpdate.id);
    return {
      groups: groups
    };
  }
  default:
    return state;
  }
};

export default groupReducer;