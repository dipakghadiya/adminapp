import { combineReducers } from 'redux';
import userReducer from './user-reducer';
import groupReducer from "./group-reducer";
const rootReducer = combineReducers({
  userReducer,
  groupReducer
});

export default rootReducer;