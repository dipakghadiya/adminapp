import React, {PureComponent} from 'react';
import TextField from '@material-ui/core/TextField';
import "./user-list-panel.css";
import CheckboxGroup from "../common/checkbox-group";
import {connect} from "react-redux";
import {addUser, editUser} from "../redux/actions/user-actions";
import FormDialog from "../common/form-dialog";


export class AddUserDialog extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      formData: {
        id: (new Date()).getTime(),
        name: '',
        groups: []
      },
      validationError: ''
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {record} = nextProps;
    if (record) {
      this.setState({formData: record});
    } else {
      this.setState({
        formData: {
          id: (new Date()).getTime(),
          name: '',
          groups: [],
          error: ''
        }
      });
    }
  }

    handleFieldChange = (field, value) => {
      let formData = {...this.state.formData};
      formData[field] = value;
      this.setState({formData, validationError: ''});
    };

    validateFormData = (formData) => {
      let {validationError} = this.state;
      if (!formData.name || formData.name === '') {
        validationError = "Name can not be null";
      } else if (formData.groups.length === 0) {
        validationError = "User needs to be assign to minimum one group";
      } else {
        this.setState({validationError: ''});
        return true;
      }
      this.setState({validationError: validationError});
      return false;
    };

    handleSave = () => {
      const {action, editUser, addUser, handleClose} = this.props;
      const {formData} = this.state;

      if (this.validateFormData(formData)) {
        if (action === 'edit') {
          editUser(formData);
        } else {
          addUser(formData);
        }
        handleClose();
      }
    };

    render() {
      const {open, handleClose, groups: groupsAvailable} = this.props;
      const {formData: {name, groups}, validationError} = this.state;

      return <FormDialog
        open={open}
        handleClose={handleClose}
        handleSave={this.handleSave}
        title="User"
        validationError={validationError}>
        <TextField
          margin="dense"
          id="name"
          label="User Name"
          type="text"
          value={name}
          required
          onChange={(event) => this.handleFieldChange('name', event.target.value)}
          fullWidth
        />
        <div className="spacer"></div>
        <CheckboxGroup data={groupsAvailable} selectedData={groups}
          onChange={(value) => this.handleFieldChange('groups', value)}/>
      </FormDialog>;
    }

}


const mapStateToProps = state => ({
  groups: state.groupReducer.groups
});
export default connect(mapStateToProps, {addUser, editUser})(AddUserDialog);