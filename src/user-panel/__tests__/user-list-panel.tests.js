import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import {UserListPanel} from "../user-list-panel";
import {userList} from "../../data/user-data";
import {groupList} from "../../data/group-data";

const mockFunction = jest.fn();

describe('Snapshot testing', () => {

  it('render list correctly for no data', () => {
    const wrapper = shallow(<UserListPanel searchValue={''}
      openDialog={mockFunction}
      deleteUser={mockFunction}
      data={[]}
      groupsData={[]}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render list correctly for no search data', () => {
    const wrapper = shallow(<UserListPanel searchValue={'search'}
      openDialog={mockFunction}
      deleteUser={mockFunction}
      data={[]}
      groupsData={[]}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render list correctly for data', () => {
    const wrapper = shallow(<UserListPanel searchValue={''}
      openDialog={mockFunction}
      deleteUser={mockFunction}
      data={userList}
      groupsData={groupList}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});

describe('Function testing', () => {


  let wrapper, instance;
  beforeEach(() => {
    wrapper = shallow(<UserListPanel searchValue={''}
      openDialog={mockFunction}
      deleteUser={mockFunction}
      data={userList}
      groupsData={groupList}
    />);
    instance = wrapper.instance();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
  it('onDeleteClick', () => {
    const event = {
      stopPropagation: jest.fn()
    };
    const user = {id: 1, name: "Test"};
    instance.onDeleteClick(event, user);
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(mockFunction).toHaveBeenCalledWith(user);
  });

  it('onEditClick', () => {
    const event = {
      stopPropagation: jest.fn()
    };
    const user = {id: 1, name: "Test"};
    instance.onEditClick(event, user);
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(mockFunction).toHaveBeenCalledWith('user', user, 'edit');
  });
});
