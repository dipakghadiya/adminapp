import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import {groupList} from "../../data/group-data";
import {AddUserDialog} from "../add-user-dialog";


const mockFunction = jest.fn();
const mockUser = {
  id: 1,
  name: "Test",
  groups: [1, 2, 3, 4]
};

describe('Snapshot testing', () => {

  it('render dialog for open', () => {
    const wrapper = shallow(<AddUserDialog open={true}
      record={mockUser}
      handleClose={mockFunction}
      groups={groupList}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render dialog for close', () => {
    const wrapper = shallow(<AddUserDialog open={false}
      record={mockUser}
      handleClose={mockFunction}
      groups={groupList}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('render dialog for validationError', () => {
    const wrapper = shallow(<AddUserDialog open={true}
      record={mockUser}
      handleClose={mockFunction}
      groups={groupList}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
    wrapper.setState({validationError: 'Some Error'});
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});

describe('Function testing', () => {
  let wrapper, instance;
  let editUserMock = jest.fn();
  let addUserMock = jest.fn();
  beforeEach(() => {
    wrapper = shallow(<AddUserDialog open={true}
      record={mockUser}
      handleClose={mockFunction}
      groups={groupList}
      editUser={editUserMock}
      addUser={addUserMock}
    />);
    instance = wrapper.instance();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
  it('handleFieldChange', () => {
    instance.handleFieldChange('name', 'User1');
    expect(instance.state.formData.name).toEqual('User1');
    expect(instance.state.validationError).toEqual('');
  });

  it('validateFormData for no validation error', () => {
    const response = instance.validateFormData(mockUser);
    expect(response).toEqual(true);
    expect(instance.state.validationError).toEqual('');
  });

  it('validateFormData for validation error for user name', () => {
    const response = instance.validateFormData({id: 1, name: '', groups: [1, 2]});
    expect(response).toEqual(false);
    expect(instance.state.validationError).toEqual("Name can not be null");
  });


  it('validateFormData for validation error for groups', () => {
    const response = instance.validateFormData({id: 1, name: 'User1', groups: []});
    expect(response).toEqual(false);
    expect(instance.state.validationError).toEqual("User needs to be assign to minimum one group");
  });


  it('handleSave for validation error', () => {
    instance.validateFormData = jest.fn();
    instance.validateFormData.mockReturnValue(false);
    instance.handleSave();
    expect(mockFunction).toHaveBeenCalledTimes(0);
    expect(addUserMock).toHaveBeenCalledTimes(0);
    expect(editUserMock).toHaveBeenCalledTimes(0);
  });

  it('handleSave for add and no validation error', () => {
    wrapper.setProps({action: 'add'});
    instance.validateFormData = jest.fn();
    instance.validateFormData.mockReturnValue(true);
    instance.handleSave();
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(addUserMock).toHaveBeenCalledTimes(1);
    expect(addUserMock).toHaveBeenCalledWith(mockUser);
    expect(editUserMock).toHaveBeenCalledTimes(0);
  });

  it('handleSave for edit and no validation error', () => {
    wrapper.setProps({action: 'edit'});
    instance.validateFormData = jest.fn();
    instance.validateFormData.mockReturnValue(true);
    instance.handleSave();
    expect(mockFunction).toHaveBeenCalledTimes(1);
    expect(addUserMock).toHaveBeenCalledTimes(0);
    expect(editUserMock).toHaveBeenCalledTimes(1);
    expect(editUserMock).toHaveBeenCalledWith(mockUser);
  });
});
