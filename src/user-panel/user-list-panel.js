import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import TagArea from "../common/tag-area";
import ListItemSummary from "../common/list-item-summary";
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import {deleteUser} from "../redux/actions/user-actions";

import "./user-list-panel.css";

export class UserListPanel extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      expanded: '',
      userList: props.data
    };
  }

    handleChange = panel => (event, newExpanded) => {
      this.setState({expanded: newExpanded ? panel : false});
    };

    onEditClick = (event, user) => {
      event.stopPropagation();
      this.props.openDialog('user', user, 'edit');
    };

    onDeleteClick = (event, user) => {
      event.stopPropagation();
      this.props.deleteUser(user);
    };

    render() {
      const {data: userList, searchValue, groupsData} = this.props;
      const {expanded} = this.state;
      const data = userList.filter(user => user.name.toLowerCase().includes(searchValue.toLowerCase()));
      const noDataMessage = searchValue ? 'No search result present' : 'No Data available';

      return (
        data.length > 0 ?
          data.map(user => {
            const groups = groupsData.filter(group=> user.groups.includes(group.id));
            return <ExpansionPanel key={user.name} square expanded={expanded === user.name}
              onChange={this.handleChange(user.name)}>
              <ExpansionPanelSummary id="panel1d-header" className="item-summary">
                <Edit onClick={(event) => this.onEditClick(event, user)}/>
                <Delete onClick={(event) => this.onDeleteClick(event, user)}/>
                <ListItemSummary title={user.name}/>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails className="item-details">
                <TagArea data={groups} label="User in Groups"/>
              </ExpansionPanelDetails>
            </ExpansionPanel>;

          }) : <span className="no-data-message">{noDataMessage}</span>
      );
    }

}

const mapStateToProps = state => ({
  data: state.userReducer.users,
  groupsData: state.groupReducer.groups
});
export default connect(mapStateToProps, {deleteUser})(UserListPanel);