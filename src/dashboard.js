import React, {PureComponent} from 'react';
import AppBar from '@material-ui/core/AppBar';
import "./common/tab-menu.css";
import UserListPanel from "./user-panel/user-list-panel";
import GroupListPanel from "./group-panel/group-list-panel";
import AddUserDialog from "./user-panel/add-user-dialog";
import AddGroupDialog from "./group-panel/add-group-dialog";
import TabMenu from "./common/tab-menu";

export default class Dashboard extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      searchValue: '',
      openDialog: false,
      type: '',
      record: undefined,
      action:''
    };
  }

    handleChange = (event, newValue) => {
      if (newValue !== undefined) {
        this.setState({value: newValue, searchValue: ''});
      }
    };

    onSearch = (event) => {
      const searchValue = (event.target.value || "").trim();
      this.setState({searchValue});
    };

    openDialog = (type, record, action) => {
      this.setState({openDialog: true, type: type, action: action, record: record});
    };

    closeDailog = () => {
      this.setState({openDialog: false, type: '', record: undefined, action:''});
    };

    render() {
      const {openDialog, type, record, action, value, searchValue} = this.state;
      return (
        <div>
          <AppBar position="static" color="default">
            <TabMenu
              openDialog={this.openDialog}
              value={value}
              searchValue={searchValue}
              onSearch={this.onSearch}
              handleChange={this.handleChange}
            />
            {value === 0 ?
              <UserListPanel searchValue={searchValue} openDialog={this.openDialog}/>
              :
              <GroupListPanel searchValue={searchValue} openDialog={this.openDialog}/>
            }
          </AppBar>
          <AddUserDialog open={openDialog && type === 'user'}
            record={record}
            action={action}
            handleClose={this.closeDailog}/>
          <AddGroupDialog open={openDialog && type === 'group'}
            record={record}
            action={action}
            handleClose={this.closeDailog}/>
        </div>
      );
    }

}