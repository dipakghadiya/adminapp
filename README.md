# AdminApp

Project is developed in ReactJs to list out User and groups.

*Steps to run this project is as below*

>* check out the project code
>* run "npm install"
>* run "npm start"
>* Hit "http://localhost:3000" to run application in chrome.

To run the test cases *run "npm run test"*

To run the eslint *run "npm run lint"*

I have use redux as my single data source here.
I have assumed that endpoints are available for those operations
1. getAllUser
2. getAllGroup
3. addUser
4. modifyUser
5. deleteUser
6. addGroup
7. modifyGroup
8. deleteGroup


